async function fetchJSON() {
  // http://192.168.101.128/fib/20
  // http://back.outsider.com/fib/20
  let response = await fetch('http://back.outsider.com/fib/20');

  console.log(response.status); // 200
  console.log(response.statusText); // OK

  if (response.status === 200) {
      let data = await response.json();
      // handle data
      console.log(data);
      let container = document.querySelector('.responsetime');
      container.innerHTML = data["timeTaken"];
  }
}

var timer = setInterval(fetchJSON,1000)

fetchJSON();